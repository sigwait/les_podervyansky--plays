#!/usr/bin/env ruby
# coding: utf-8

require 'pp'

$conf = {
  act_count: 0,
  chars: {},
}

def dialogue line
  return nil unless $conf[:chars][:end]

  $conf[:chars].each do |k,v|
    if m = line.match(/^#{k} ( *\((.+)\).?) *(.+)/i)
      r = "<p class='my-ch'>#{k}</p>\n"
      r += "<p class='my-ch-stage'>#{m[2]}</p>\n"
      r += "<p>#{m[3]}</p>\n\n"

      return {print: true, line: r}
    end

    if m = line.match(/^#{k}[:\.] *(.+)/i)
      r = "<p class='my-ch'>#{k}</p>\n"
      r += "<p>#{m[1]}</p>\n\n"

      return {print: true, line: r}
    end

  end

  nil
end

def act line
  if line.match(/^Д[іi]я [[:word:]]+\.?$/)
    $conf[:act_count] += 1
    return {print: true, line: "\n<h2>ACT #{$conf[:act_count]}</h2>\n\n"}
  end

  nil
end

def myset line
  return {print: true, line: "<p class='my-set'>#{line}</p>\n\n"} if line != ""
  {print: true, line: ""}
end

def chars line
  return nil if $conf[:chars][:end]

  if !$conf[:chars][:start] && line.match(/^Дійові особи/)
    $conf[:chars][:start] = true
    return {print: false}
  end

  return nil unless $conf[:chars][:start]

  if line.match(/^\s*$/)
    $conf[:chars][:end] = true
    return nil
  end

  m = line.match(/^([^,]+), *(.+)\.?$/)
  return {print: true, line: line} unless m
  $conf[:chars][m[1]] = m[2]

  return {print: false}
end

def chars_print
  puts '	<table class="my-ch-table">
	  <tbody>'

  $conf[:chars].each do |k,v|
    puts "        <tr>
          <td>#{k}</td>
          <td>#{v}</td>
        </tr>\n\n"
  end

  puts '	  </tbody>
	</table>'

end

while line = $stdin.gets
  r = nil
  [:act, :chars, :dialogue, :myset].each do |func|
    r = send func, line.strip
    break if r
  end

  if r && r[:print]
    puts r[:line]
  elsif r && !r[:print]
    # do nothing
  else
    puts line
  end
end

chars_print
