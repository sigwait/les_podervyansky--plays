#!/usr/bin/env ruby
# coding: utf-8

$conf = {
  act_count: 0
}

def dialogue line
  if r = line.match(/([^.]+)\. *(.+)/)
    line = "<p class='my-ch'>#{r[1]}</p>"
    line += "\n<p>#{r[2]}</p>\n\n"

    line.gsub!(/\((.+?)\)/, "</p>\n<p class='my-ch-stage'>\\1</p>\n<p>")
    line.gsub!(/<p><\/p>\n*/, '')
    return line
  end

  nil
end

def act line
  if line.match(/^Дiя [[:word:]]+\.$/)
    $conf[:act_count] += 1
    return line = "\n<h2>ACT #{$conf[:act_count]}</h2>\n\n"
  end

  nil
end

while line = $stdin.gets
  r = nil
  [:act, :dialogue].each do |func|
    r = send func, line
    break if r
  end

  puts r ? r : line
end
