#!/usr/bin/env ruby
# coding: utf-8

require 'pp'

$conf = {
  act_count: 0,
  chars: {},

  d: nil
}

def dialogue line
  return nil unless $conf[:chars][:end]

  # char dialog end
  if line.strip == "" && $conf[:d]
    $conf[:d] = nil
    return {print: true, line: "</p>\n\n"}
  end

  $conf[:chars].each do |k,v|
    if m = line.match(/^#{k} ( *\((.+)\).?)$/i)
      r = ''#"</p>\n\n"
      r += "<p class='my-ch'>#{k}</p>\n"
      r += "<p class='my-ch-stage'>#{m[2]}</p>\n"
      r += "<p>\n"

      $conf[:d] = k
      return {print: true, line: r}
    end

    if m = line.match(/^#{k}[:\.]?$/i)
      r = ''#"</p>\n\n"
      r += "<p class='my-ch'>#{k}</p>\n"
      r += "<p>\n"

      $conf[:d] = k
      return {print: true, line: r}
    end

  end

  if $conf[:d]
    if m = line.match(/^\((.+)\)/)
      return {print: true, line: "</p>\n<p class='my-ch-stage'>#{m[1]}</p>\n<p>\n"}
    else
      return {print: true, line: "#{line}<br>\n"}
    end
  end

  nil
end

def act line
  if line.match(/^Д[іi]я [[:word:]]+\.?$/)
    $conf[:act_count] += 1
    return {print: true, line: "\n<h2>ACT #{$conf[:act_count]}</h2>\n\n"}
  end

  nil
end

def chars line
  return nil if $conf[:chars][:end]

  if !$conf[:chars][:start] && line.match(/^Дійові особи/)
    $conf[:chars][:start] = true
    return {print: false}
  end

  return nil unless $conf[:chars][:start]

  if line.match(/^\s*$/)
    $conf[:chars][:end] = true
    return nil
  end

  m = line.match(/^([^,]+), *(.+)\.?$/)
  return {print: true, line: line} unless m
  $conf[:chars][m[1]] = m[2]

  return {print: false}
end

def myset line
  return {print: true, line: "<p class='my-set'>#{line}</p>\n\n"} if line != ""
  {print: true, line: ""}
end

def chars_print
  puts '	<table class="my-ch-table">
	  <tbody>'

  $conf[:chars].each do |k,v|
    puts "        <tr>
          <td>#{k}</td>
          <td>#{v}</td>
        </tr>"
  end

  puts '	  </tbody>
	</table>'

end

while line = $stdin.gets
  r = nil
  [:act, :chars, :dialogue, :myset].each do |func|
    r = send func, line.strip
    break if r
  end

  if r && r[:print]
    puts r[:line]
  elsif r && !r[:print]
    # do nothing
  else
    puts line
  end
end

chars_print
