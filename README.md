# Les Podervyansky's plays in the Standard Stage Play Format

## Requirements

* GNU Make
* Ruby 2.3+, `gem install nokogiri twitter_cldr`
* Calibre 3.7+
* `npm -g i epub-hyphen`
* ghostscript
* kindlegen

## Compile

	$ make -f main.mk

The resulting `_out` dir should look like:

~~~
_out/
├── .cache/
├── web/
├── les_podervyansky--plays.epub
├── les_podervyansky--plays.mobi
├── les_podervyansky--plays.pdf
└── web.zip
~~~

`web` dir contains standalone .html files (1 file == 1 play). `.cache`
contains tmp build files you may ignore.


## Contributing

If you want to fix/add a play, edit files in `src/` dir, then rerun
make. **Don't edit .txt files** in `data-raw` dir, they're there
mostly for hist. purposes.


## TODO

* transcribe .mp3 files in `data-raw`
* change the cover to smthg more prettier


## License

(For the build scripts only, not for the book content.)

MIT.
