.DELETE_ON_ERROR:

.PHONY: compile
compile:

SHELL := $(shell which bash)
mk := $(dir $(lastword $(MAKEFILE_LIST)))
src := $(mk)/src
out := _out

# canned recipes
mkdir = @mkdir -p $(dir $@)
copy = cp $< $@



out.web := $(out)/web
web.src := $(wildcard $(src)/main.css $(src)/*.html $(src)/*.jpg $(src)/*.js)
web.dest := $(patsubst $(src)/%, $(out.web)/%, $(web.src))

$(web.dest): $(out.web)/%: $(src)/%
	$(mkdir)
	$(copy)

compile: $(web.dest)



html.src := $(sort $(filter %.html, $(web.dest)) \
	$(out.web)/ch98.names-index.meta.html \
	$(out.web)/ch98.plays-index.meta.html)
# play chapters only
html.ch.src := $(filter-out %.meta.html, $(html.src))

$(out.web)/ch98.%-index.meta.html: $(src)/ch98.%-index.meta.erb $(html.ch.src)
	$(mk)/cast $* $(html.ch.src) | erb -r json $(src)/$(basename $(notdir $@)).erb > $@

$(out.web)/index.html: $(src)/metadata.xml $(src)/index.erb $(html.src)
	$(mk)/toc $^ > $@

# requires bash process substitution
$(out.web)/index.json: $(src)/metadata.xml $(html.src)
	$(mk)/toc $(src)/metadata.xml <(echo '<%= require "json"; toc[:body].to_json %>') $(html.src) > $@



compile.web := $(web.dest) $(out.web)/index.html $(out.web)/index.json
.PHONY: web
web: $(compile.web)

$(out)/web.zip: $(compile.web)
	cd $(dir $<) && zip -0 -q $(CURDIR)/$@ *

book.name := $(notdir $(CURDIR))
cache := $(out)/.cache

$(cache)/$(book.name).epub: $(out)/web.zip $(src)/print.css $(src)/cover.jpg $(src)/metadata.xml
	$(mkdir)
	ebook-convert $< $@ \
	 --chapter '/' \
	 --page-breaks-before '/' \
	 --level1-toc '//*[contains(concat(" ", @class, " "), " title ")]' \
	 --dont-split-on-page-breaks \
	 \
	 --change-justification left \
	 --minimum-line-height 0 \
	 --smarten-punctuation \
	 --no-svg-cover \
	 --extra-css $(src)/print.css \
	 --cover $(src)/cover.jpg \
	 -m $(src)/metadata.xml

$(out)/$(book.name).epub: $(cache)/$(book.name).epub
	epub-hyphen $< -o $@

compile: $(out)/$(book.name).epub



$(out)/$(book.name).mobi: $(out)/$(book.name).epub
# kindlegen 2.9 fails if $@ contains a dir component
	cd $(dir $@) && kindlegen $(notdir $<) -o $(notdir $@)

compile: $(out)/$(book.name).mobi



$(cache)/$(book.name).pdf: $(out)/$(book.name).epub
	ebook-convert $< $@ \
	 --chapter '/' \
	 --pdf-add-toc \
	 --pdf-footer-template '<small style="position: absolute; bottom: 2em; left: 0; text-transform: uppercase;"><code>_SECTION_</code></small><small style="position: absolute; bottom: 2em; right: 0;"><code>_PAGENUM_</code></small>' \
	 --preserve-cover-aspect-ratio \
	 --pdf-serif-family "Times New Roman" \
	 --pdf-mono-family "Courier New" \
	 --paper-size a4

# set the default view; requires bash process substitution
$(out)/$(book.name).pdf: $(cache)/$(book.name).pdf
	gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=$@ $< <(echo [ /PageMode /UseOutlines /Page 2 /DOCVIEW pdfmark)

compile: $(out)/$(book.name).pdf
