'use strict';

/*
  [ 0 name ] [ 1 desc ]
  [ 2 name ] [ 3 desc ]
  [ 4 name ] [ 5 desc ]

  Retrieve the character desc either by NAME or ID. Return null on no matches.
*/
let ch_match = function(name, id, mode = 'exact') {
    let td = document.querySelectorAll('.my-ch-table tr td')
    if (!td.length) return null
    if (name) name = name.trim()

    for (let idx = 0; idx < td.length; idx += 2) {
	let cname = td[idx].innerText.trim()
	let r = () => ({ name: cname, desc: td[idx+1].innerText })

	if (id !== undefined) {
	    if (id == idx) return r() // == because id could be a string
	} else {
	    switch (mode) {
	    case 'exact':
		if (name === cname) return r()
		break
	    default:
		if (new RegExp(name, 'i').test(cname)) return r()
	    }
	}
    }

    // retry 1 more time w/ a more lax search
    if (mode === 'exact') return ch_match(name, id, 'regexp')
    return null			// give up
}

let Popup = class {
    constructor(event, html) {
	this.event = event
	this.html = html

	this.destroy = this._destroy.bind(this)
	this.kbd_hook = this._kbd_hook.bind(this)
    }

    show() {
	this.win = document.createElement('div')
	let w = this.win.style
	w.position = 'fixed'
	w.left = this.event.clientX - 5 + 'px'
	w.top = this.event.clientY - 5 + 'px'
	w.right = 'auto'
	w.bottom = 'auto'

	w.maxWidth = '20em'
	w.padding = '0.8em'
	w.border = '1px solid lightgray'
	w.fontSize = 'normal'
	w.textAlign = 'left'
	w.backgroundColor = '#FEFEFE'
	w.color = 'black'
	w.boxShadow = '0px 0px 10px rgba(0, 0, 0, 0.6)'
	w.borderRadius = '3px'
	w.cursor = 'pointer'

	document.body.appendChild(this.win)
	this.win.innerHTML = ['<div>', this.html, '</div>'].join("\n")

	let offset = this.offset_if_not_visible(this.win)
	if (offset.x_hid > 0) w.left = this.win.offsetLeft - offset.x_hid + 'px'
	if (offset.y_hid > 0) w.top = this.win.offsetTop - offset.y_hid + 'px'

	document.addEventListener('keydown', this.kbd_hook)
	document.addEventListener('mousedown', this.destroy)
	document.addEventListener('scroll', this.destroy)
    }

    offset_if_not_visible(win) {
	let x_vis = window.innerWidth - win.offsetLeft
	let x_hid = win.offsetWidth - x_vis

	let y_vis = window.innerHeight - win.offsetTop
	let y_hid = win.offsetHeight - y_vis

	return {x_hid, y_hid}
    }

    _destroy() {
	document.removeEventListener('keydown', this.kbd_hook)
	document.removeEventListener('mousedown', this.destroy)
	document.removeEventListener('scroll', this.destroy)
	document.body.removeChild(this.win)
    }

    _kbd_hook(event) {
	if (/^Esc/.test(event.key)) this.destroy()
    }
}

// main
document.addEventListener('DOMContentLoaded', () => {
    // EdgeHTML 15.15063 still needs this
    NodeList.prototype[Symbol.iterator] = Array.prototype[Symbol.iterator]

    let chs = document.querySelectorAll('.my-ch')
    for (let val of chs) {
	val.style.cursor = 'pointer'
	val.onclick = (evt) => {
	    let chs = []
	    let dataset = val.id.replace(/^data-/, '')
	    if (dataset) {
		chs = dataset.split('-').map( val => ch_match(null, val))
	    } else {
		chs = [ch_match(val.innerText)]
	    }
	    chs = chs.filter( val => val)
	    let html = ['No such character in the table.']
	    if (chs.length) html = chs.map( ch => `<b>${ch.name}</b> -- ${ch.desc || "(no description)"}`)

	    new Popup(evt, html.join('<br>')).show()
	}
    }
})
