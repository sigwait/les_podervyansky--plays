'use strict';

class Nav {
    constructor() {
	this.dom = {}

	if (!(this.dom.h1 = document.querySelector('h1.title')))
	    throw new Error('no h1.title')
	this.dom.h1.insertAdjacentElement('afterend', this.mkpanel())
    }

    mkpanel() {
	let div = document.createElement('div')
	div.innerHTML = `\
<div class="noprint" style="display: table; width: 100%">
  <div style="display: table-cell; width: 50%"></div>
  <div style="display: table-cell; width: 50%"></div>
</div>`
	return this.dom.menu = div.firstChild
    }

    pcell(loc) {
	return this.dom.menu.querySelectorAll('div')[loc === 'left' ? 0 : 1]
    }

    link(href, text, title) {
	return `<a style="text-decoration: none" href="${href}" title="${title}">${text}</a>`
    }

    mkbuttons(toc) {
	let file = window.location.pathname.match(/[^/]+$/)[0]
	let cur = toc.findIndex( val => val.url === file)

	let up = this.link('index.html', '&uArr;', 'TOC')
	let prev = cur > 0 ?
	    this.link(toc[cur-1].url, '&lArr;', toc[cur-1].name) : null
	let next = cur >= 0 && cur < toc.length-1 ?
	    this.link(toc[cur+1].url, '&rArr;', toc[cur+1].name) : null

	let btn = this.pcell('left')
	btn.innerHTML = [prev, up, next].filter(val => val).join('&nbsp;&nbsp;')
    }

    // hijack h1.title attr & inject it into the right cell
    mkauthor() {
	let w = this.pcell('right')
	w.style.textAlign = 'right'
	w.innerHTML = `<small>${this.dom.h1.title}</small>`
	this.dom.h1.title = ''
    }
}

// main
document.addEventListener('DOMContentLoaded', () => {
    if (navigator.epubReadingSystem) return

    let nav = new Nav()
    nav.mkauthor()

    fetch('index.json').then( res => res.json()).then( toc => {
	nav.mkbuttons(toc)
    })
})
