#!/usr/bin/env ruby

require 'time'
require 'erb'
require 'nokogiri'

raise "Usage: #{$0} meta.xml template.erb ch00.html ..." if ARGV.size < 3

def meta file
  doc = File.open(file) { |f| Nokogiri::XML(f) }
  doc.remove_namespaces!
  {
    title: doc.css('title').text,
    subtitle: doc.css('description').text,
    creator: doc.css('creator').text,
  }
end

def toc files
  r = []
  files.each do |file|
    doc = File.open(file) { |f| Nokogiri::HTML(f) }
    node = doc.css('h1.title')
    r << {
      klass: node.first["class"].match(/\bunnumbered\b/) ? 'unnumbered' : '',
      url: File.basename(file),
      name: node.text,
    }
  end
  r
end


toc = meta ARGV[0]
ARGV.shift
template = File.read ARGV[0]
ARGV.shift

toc[:body] = toc ARGV

puts ERB.new(template).result
