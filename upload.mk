out := _out

.PHONY: upload
upload:
	rsync -avPL --delete -e ssh --exclude=.cache/ $(out)/ gromnitsky@frs.sourceforge.net:/home/user-web/gromnitsky/htdocs/lit/les_podervyansky
